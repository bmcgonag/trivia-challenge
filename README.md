# Welcome to Trivia-Challege

An online / self-hostable - multi-player trivia game where you control the questions, answers, and options for the game.

## Documentation

The Gitlab Wiki for this repo has the install and setup instructions, and game play instructions.  You can find them at
- [Trivia Challenge Wiki](https://gitlab.com/bmcgonag/trivia-challenge/-/wikis/Trivial-Challenge---Home)

I hope you enjoy the game, and I'll be trying to improve it as I go, so check back for updates.  If you'd like to contribute feel free to clone, and make a pull request. 

