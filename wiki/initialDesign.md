"results": [{
			"category": "Animals",
			"type": "boolean",
			"difficulty": "easy",
			"question": "The Axolotl is an amphibian that can spend its whole life in a larval state.",
			"correct_answer": "True",
			"incorrect_answers": ["False"]
		}, {
			"category": "Science & Nature",
			"type": "multiple",
			"difficulty": "easy",
			"question": "Which Apollo mission was the first one to land on the Moon?",
			"correct_answer": "Apollo 11",
			"incorrect_answers": ["Apollo 10", "Apollo 9", "Apollo 13"]
		}

### Game Views:
	- [x] Login
	- [x] Select Game
		- [x] Create Game URL (automatically)
		- [x] Create Route (automatically)
	- [x] Game mode
		- [x] All Answered (all teams answer before moving to next question)
			- [x] Total points win - show winner at end.
			- [] Timer based points - fastest correct gets most points, 2nd fastest fewer, and so on.
		- [] Timer based (fastest team wins, then move on)
			- [] Total points win in the end.
				- [] Show 1st, 2nd, 3rd, ...
	- Game Settings
		Answer Type
		- [x] All Multiple Choice
		- [x] All True / False
		- [x] Mixed
		Difficulty Level
		- [x] Easy
		- [x] Medium
		- [x] Hard
		- [] Mixed
		Category
		- [x] choose 1, multiple categories
			- [] Choose number of questions to pull from each category (optional)
			- [x] Random number from each category
		- [x] Mixed
	- [x] Distribute URL or Invite Users
	- Join Game view
		- [] Choose Team name
		- [x] List other teams already joined
		- [x] Show teams joining
		- [] Ready button (all teams should press to indicate ready)
	- Game Views
		2 options
			1. [x] Question and answer options on same screen
			2. [] Question on main screen with devices as answer pads only 

### Game History View:
	- [] Game Settings selected
	- [] Date started
	- [] Date finished
	- [] Participants
		- [] Participant Answers to each Question
	- [] Questions Asked
		- [] Correct answers
		- [] Incorrect answers
	- [] Winning Team / Participant

### New System Setup:
	- [] Import of Questions and Answers from CSV
	- [x] Manual entry of questions and answers
	- [x] Allow users to create their own question categories to choose from during game settings.
