# My notes for this project

This is turning into a great tool for helping my kids study for quizzes and tests.  While it's limited to Multiple Choice and True/False, it is great to have them do the work to create the questions they think will be on a quiz / test.  Then we sit together and put them in.  

Creativity in making the questions / answers fit into a short format ABCD or TF style really helps.  They have to think about it, and a lot of times realize a question has more than 1 answer, so they make multiple game questions out of it. 

We are using the categories to separate questions into classes, and even into class sections from their books. I can update the categories later (right now just through direct db manipulation) if we need to free up category list space. 

For extra study guides when they do poorly on a game, they use the 'Print Cards' view, which can now be filtered by category as well. 

I want to build out the ability to bulk rename categories (you can edit a category name, but I don't have it hooked up to rename the category is it's already applied to a question.)
