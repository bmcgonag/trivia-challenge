
Template.login.onCreated(function() {

});

Template.login.onRendered(function() {
    
});

Template.login.helpers({
    areFilled: function() {
        return Session.get("filledFields");
    },
    misEmail: function() {
        return Session.get("misEmail");
    },
    misPass: function() {
        return Session.get("misPass");
    },
});

Template.login.events({
    'click #logmein' (event) {
        event.preventDefault();
        // console.log("clicked login");
        let email = $("#email").val();
        let pass = $("#password").val();
        let emailMiss = false;
        let passMiss = false;

        if (email == null || email == "") {
            emailMiss = true;
            Session.set("misEmail", true);
        }

        if (pass == null || pass == "") {
            passMiss = true;
            Session.set("misPass", true);
        }

        if (emailMiss == true || passMiss == true) {
            // console.log("Fields not filled");
            Session.set("filledFields", false);
        } else {
            Session.set("filledFields", true);
            Meteor.loginWithPassword(email, pass, function(err, result) {
                if (err) {
                    console.log("Error Loggin In: " + err);
                } else {
                    FlowRouter.go('/gamePlay');
                }
            });
        }
    },
    'click #reg' (event) {
        event.preventDefault();
        FlowRouter.go('/reg');
    },
});