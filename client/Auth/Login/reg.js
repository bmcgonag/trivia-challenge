import { Accounts } from 'meteor/accounts-base';

Template.reg.onCreated(function() {

});

Template.reg.onRendered(function() {
    Session.set("missingReq", false);
    Session.set("missingName", false);
    Session.set("missingPhone", false);
    Session.set("missingEmail", false);
    Session.set("missingPassword", false);
    Session.set("missingPasswordConfirm", false);
    Session.set("missingUsername", false);
});

Template.reg.helpers({
    misName: function() {
        return Session.get("missingName");
    },
    misEmail: function() {
        return Session.get("missingEmail");
    },
    misPass: function() {
        return Session.get("missingPassword");
    },
    misPassConfirm: function() {
        return Session.get("missingPasswordConfirm");
    },
    misReq: function() {
        return Session.get("missingReq");
    },
    misUsername: function() {
        return Session.get("missingUsername");
    },
});

Template.reg.events({
    'click #registerMe' (event) {
        event.preventDefault();
        console.log("Clicked");
        let missingName = false;
        let missingEmail = false;
        let missingPassword = false;
        let missingPasswordConfirm = false;
        let missingUsername = false;

        let email = $("#email").val();
        let password = $("#password").val();
        let name = $("#name").val();
        let confPass = $("#passwordConfirm").val();
        let username = $("#username").val();

        if (name == "" || name == null) {
            missingName = true;
            Session.set("missingName", true);
        }

        if (email == "" || email == null) {
            missingEmail = true;
            Session.set("missingEmail", true);
        }

        if (username == "" || username == null) {
            missingUsername = true;
            Session.set("missingUsername", true);
        }

        if (password == "" || password == null) {
            missingPassword = true;
            Session.set("missingPassword", true);
        }

        if (confPass == "" || confPass == null) {
            missingPasswordConfirm = true;
            Session.set("missingPasswordConfirm", true);
        }

        let userId;

        if (missingName == true || missingEmail == true || missingUsername == true || missingPassword == true || missingPasswordConfirm == true) {
            Session.set("missingReq", true);
        } else {
            Session.set("missingReq", false);
            Accounts.createUser({
                email: email,
                username: username,
                password: password,
                profile: {
                    fullname: name,
                }
            });

            console.log("Successfully registered user.");
            Meteor.call("addToRole", "player", function(err, result) {
                if (err) {
                    console.log("    ERROR: ROLES - Error adding user to role: " + err);
                } else {
                    // console.log("User should be added to role - teacher.");
                    FlowRouter.go('/gamePlay');
                }
            });
        }
    },
    'keyup #passwordConfirm' (event) {
        let pwd = $("#password").val();
        let pwdconf = $("#passwordConfirm").val();

        if (pwd == pwdconf) {
            // console.log("passwords match");
            Session.set("canreg", true);
        } else {
            // console.log("passwords don't match");
            Session.set("canreg", false);
        }
    },
    'change #email' (event) {
        let email = $("#email").val();
        var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        let isEmail = regex.test(email);
        if (isEmail == false) {
            Session.set("missingEmail", true);
        } else {
            Session.set("missingEmail", false);
        }
    },
    'click #login' (event) {
        event.preventDefault();
        FlowRouter.go('/login');
    },
});