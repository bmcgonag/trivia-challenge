
Template.myProfile.onCreated(function() {

});

Template.myProfile.onRendered(function() {

});

Template.myProfile.helpers({
    myInfo: function() {
        return Meteor.users.find({});
    },
    myName: function() {
        return this.profile.fullname;
    },
    myEmail: function() {
        return this.emails[0].address;
    },
});

Template.myProfile.events({
    'click #editMyEmail' (event) {
        Session.set("emailEdit", true);
        Session.set("editEmailCancel", true);
    },
    'click #editMyName' (event) {
        Session.set("nameEdit", true);
        Session.set("editNameCancel", true);
    },
    'click #updatePassword' (event) {

    },
    'keyup #enterEmail' (event) {
        let email = $("#enterEmail").val();
        if (email == "" || email == null) {
            Session.set("editEmailCancel", true);
            Session.set("editEmailSave",false);
        } else {
            Session.set("editEmailCancel", false);
            Session.set("editEmailSave", true);
        }
    },
    'keyup #enterName' (event) {
        let name = $("#enterName").val();
        if (name == "" || name == null) {
            Session.set("editNameCancel", true);
            Session.set("editNameSave", false);
        } else {
            Session.set("editNameCancel", false);
            Session.set("editNameSave", true);
        }
    },
    'click #editEmailCancel' (event) {
        Session.set("emailEdit", false);
        Session.set("editEmailCancel", false);
    },
    'click #editNameCancel' (event) {
        Session.set("nameEdit", false);
        Session.set("editNameCancel", false);
    },
    'click #saveMyName' (event) {
        let name = $("#enterName").val();
        if (name == "" || name == null) {
            console.log("Cannot save a blank name.");
            showSnackbar("Name is Required!", "red");
        } else {
            Meteor.call("update.myName", name, function(err, result) {
                if (err) {
                    console.log("    ERROR updating name: " + err);
                    showSnackbar("Error Updating Name!", "red");
                } else {
                    showSnackbar("Name Updated Successfully!", "green");
                    Session.set("nameEdit", false);
                    Session.set("editNameCancel", false);
                    Session.set("editNameSave", false);
                }
            });
        }
    },
    'click #saveMyEmail' (event) {
        let email = $("#enterEmail").val();
        if (email == "" || email == null) {
            console.log("Cannot save a blank email address.");
            showSnackbar("Email is Required!", "red");
        } else {
            Meteor.call('update.myEmail', email, function(err, result) {
                if (err) {
                    console.log("    ERROR updting email: " + err);
                    showSnackbar("Error Updating Email!", "red");
                } else {
                    showSnackbar("Email Updated Successfully!", "green");
                    Session.set("emailEdit", false);
                    Session.set("editEmailCancel", false);
                    Session.set("editEmailSave", false);
                }
            });
        }
    },
    'click #updatePassword' (event) {
        event.preventDefault();
        let oldPass = $("#origPass").val();
        let newPass = $("#newPass").val();
        let newPassConf = $("#newPassConf").val();

        // check that old password and new aren't the same
        if (oldPass == newPass) {
            showSnackbar("New Password Cannot Match Old Password!", "red");
            return;
        } else if (newPass != newPassConf) {
            showSnackbar("New Passwords Do Not Match!", "red");
            return;
        } else {
            Accounts.changePassword(oldPass, newPass, function(err, result) {
                if (err) {
                    console.log("    ERROR updating password: " + err);
                    showSnackbar("Error Updating Password!", "red");
                } else {
                    showSnackbar("Password Updated Successfully!", "green");
                }
            });
        }
    }
});