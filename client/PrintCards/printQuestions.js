import { Categories } from '../../imports/api/categories.js';
import { Questions } from '../../imports/api/questions.js';

Template.printQuestions.onCreated(function() {
    this.subscribe('questions');
    this.subscribe('categories');
});

Template.printQuestions.onRendered(function() {
    Session.set("printCat", "");
    Session.set("sequence", "");
})

Template.printQuestions.helpers({
    allQuestions: function() {
        Session.set("sequence", Math.floor((Math.random() * 4) + 1));
        let catSel = Session.get("printCat");
        if (catSel != null && catSel != "") {
            return Questions.find({ "category": catSel });
        } else {
            return Questions.find({});
        }
        
    },
    sequence: function() {
        return Session.get("sequence");
    },
    catList: function() {
        return Categories.find({});
    },
});

Template.printQuestions.events({
    'change #printCat' (event) {
        let catSel = $("#printCat").val();
        Session.set("printCat", catSel);
    },
});