import { Games } from '../../../imports/api/games.js';
import { Questions } from '../../../imports/api/questions.js';
import { GameQuestions } from '../../../imports/api/gameQuestions.js';

Template.createAGame.onCreated(function() {
    this.subscribe('games');
    this.subscribe('questions');
    this.subscribe('gameQuestions');
});

Template.creatingGameView.onCreated(function() {
    Meteor.setTimeout(findQuestionsMatchingCriteria, 2000);
});

Template.createAGame.events({
    'click #saveCreateGame' (event) {
        event.preventDefault();

        var gameCode = "";
        // need to generate a unique game number
        var randomString = function(stringLength) {

            var possible = "ABCDE0FGHIJ1KLMNO2PQRST3UVWXY4Zabcd5efghi6jklmn7opqrs8tuvwx9yz0123456789";
            for(var i = 0; i < stringLength; i++) {
                gameCode += possible.charAt(Math.floor(Math.random() * possible.length));
            }
            // console.log('Game code is: ' + gameCode);
        }
        // call the function above, and pass the length you want for the random string code
        randomString(6);

        // check that certain fields are filled out
        qCat = [];
        var gameName = $("#gameName").val();
        var gameType = $("#gameType").val();
        var qType = $("#questionType").val();
        var qDifficulty = $("#questionDifficulty").val();
        var qCat = $("#questionCategories").val();
        var qTimeLimit = $("#timeLimit").val();

        if (qTimeLimit == "" || qTimeLimit == null) {
            qTimeLimit = "No Limit";
        }

        Session.set("gameCode", gameCode);
        Session.set("gameName", gameName);
        Session.set("qCat", qCat);
        Session.set("gameType", gameType);
        Session.set("qType", qType);
        Session.set("qDifficulty", qDifficulty);
        Session.set("questionTimeLimit", qTimeLimit);

        // form field validation
        if (gameType == '' || gameType == null) {
            showSnackbar("You must choose a Game Type.", "red");
            document.getElementById('gameType').style.borderColor = "red";
        } else if (gameName == '' || gameName == null) {
            showSnackbar("You need a Game Name.", "red");
            document.getElementById('gameName').style.borderColor = "red";
        } else {
            if (qCat == '' || qCat == null) {
                showSnackbar("You must choose at least one Question Category.", "red");
                document.getElementById('questionCategories').style.borderColor = "red";
            } else {
                // console.log("call find questions function.");
                findQuestionsMatchingCriteria();
            }
        }
    },
    'click #cancelCreateGame' (event) {
        event.preventDefault();
        var qCat = $("#questionCategories").val("");
        var questions = Questions.find({ category: qCat }).fetch();

        FlowRouter.go('/');
    },
});

function findQuestionsMatchingCriteria() {
    var qType = Session.get("qType");
    var qDifficulty = Session.get("qDifficulty");
    var qCat = Session.get("qCat");

    var theSeqNo = [];
    if (qType == 'mixed' && qDifficulty == 'mixed') {
        var questions = Questions.find({ category: { $in: qCat }}).fetch()
        let totalQuestions = questions.length;
        if (totalQuestions == 0) {
            showSnackbar("Your selections resluted in no questions, Try again.", "red");
            setTimeout(function(){
                FlowRouter.go("/createAGame");
            }, 3000);
            return;
        } else {
            for (i = 0; i < questions.length; i++) {
                theSeqNo[i] = questions[i].seqNo;
            }
        }
    } else if (qType == 'mixed' && qDifficulty != 'mixed') {
        var questions = Questions.find({ category: { $in: qCat }, difficulty: qDifficulty }).fetch();
        let totalQuestions = questions.length;
        if (totalQuestions == 0) {
            showSnackbar("Your selections resluted in no questions, Try again.", "red");
            setTimeout(function(){
                FlowRouter.go("/createAGame");
            }, 3000);
            return;
        } else {
            for (i = 0; i < totalQuestions; i++) {
                theSeqNo[i] = questions[i].seqNo;
            }
        }
    } else if (qDifficulty == 'mixed' && qType != 'mixed') {
        var questions = Questions.find({ type: qType, category: { $in: qCat }}).fetch();
        let totalQuestions = questions.length;
        if (totalQuestions == 0) {
            showSnackbar("Your selections resluted in no questions, Try again.", "red");
            setTimeout(function(){
                FlowRouter.go("/createAGame");
            }, 3000);
            return;
        } else {
            for (i = 0; i < questions.length; i++) {
                theSeqNo[i] = questions[i].seqNo;
            }
        }
    } else {
        var questions = Questions.find({ type: qType, category: { $in: qCat }, difficulty: qDifficulty }).fetch();
        let totalQuestions = questions.length;
        if (totalQuestions == 0) {
            showSnackbar("Your selections resluted in no questions, Try again.", "red");
            setTimeout(function(){
                FlowRouter.go("/createAGame");
            }, 3000);
            return;
        } else {
            for (i = 0; i < questions.length; i++) {
                theSeqNo[i] = questions[i].seqNo;
            }
        }
    }

    Session.set("theSeqNo", theSeqNo);
    if (theSeqNo != []) {
        writeGameToDB();
    } else {
        console.log("seq no did not get any values.");
    }
}

function writeGameToDB() {
    var gameCode = Session.get("gameCode");
    var gameName = Session.get("gameName");
    var gameType = Session.get("gameType");
    var qType = Session.get("qType");
    var qDifficulty = Session.get("qDifficulty");
    var qCat = Session.get("qCat");
    var timeLimit = Session.get("questionTimeLimit");

    $("#gameCodeSpace").append("Game Code is: " + gameCode);
    Session.set("gameCode", gameCode);
    Session.set("gameName", gameName);
    Meteor.call('newGame.insert', gameType, gameName, qType, qDifficulty, qCat, gameCode, timeLimit, function(err, result) {
        if (err) {
            showSnackbar("An error occurred saving the Game.", "red");
            Meteor.call('Error.Set', "createAGame.js", "line 142", err);
        } else {
            showSnackbar("Game Created Successfully!", "green");
            FlowRouter.go('/noOfQuestions');
        }
    });
}
